nagios_central:
  # Install Nagios central server
  pkg.installed:
    - pkgs:
      - nagios3-core
      - nagios3-common
      - nagios3-cgi
      - nagios-images
      - nagios-nrpe-server
      - nagios-nrpe-plugin
      - nagios-plugins-common
      - nagios-plugins-basic
      - nagios-plugins-standard
      # - nagios-plugins
    - install_recommends: false


nagios_central_ilo:
  # Install Nagios central ilo depedencies
  # No need for libwww-perl libwww-robotrules-perl so install_recommends: false
  cmd.run:
    - name: >
       apt-get -y --no-install-recommends install
       libnagios-plugin-perl libio-socket-ssl-perl libxml-simple-perl
    # - install_recommends: false


nagios_nrpe:
  # Install Nagios nrpe remote client
  pkg.installed:
    - pkgs:
      - nagios-nrpe-server
      # - monitoring-plugins
      # - monitoring-plugins-basic
      - nagios-plugins-common
      - nagios-plugins-basic
      - nagios-plugins-standard
      # - nagios-plugins
    - install_recommends: false

# nagios    ALL=(ALL) NOPASSWD: /usr/lib/nagios/plugins/,/usr/bin/arrayprobe
