#!/bin/sh
# Script to report on packages/files and other semi-related settings
# relevant to security status of Nagios
#
# Copyright 2018 Gary Wright http://www.wrightsolutions.co.uk
#
# Debian: apt-get --no-install-recommends install nagios3-common nagios3-core nagios3-cgi nagios-images
#

DTISO_=$(/bin/date +%Y%m%dT%H%M%S)
printf '%s Monitoring installation report\n' "${DTISO_}"

printf "\n# -v- Nagios packages installed\n"

/usr/bin/dpkg -l | /bin/egrep '^ii' | \
/bin/egrep -- 'nagios3|nagios-|monitoring-|nagios-nrpe-' | \
/usr/bin/tr -s ' ' | /usr/bin/cut -d ' ' -f1-3 | \
/usr/bin/awk -F' ' '{ print $1" "$2"        "$3 }'

printf "\n# -v- Directory permission and nagios.cmd (named pipe)\n"

/bin/vdir -d /var/lib/nagios3/
/bin/vdir -d /var/lib/nagios3/rw

#commandfile='/usr/local/nagios/var/rw/nagios.cmd'
commandfile='/var/lib/nagios3/rw/nagios.cmd'

# Next is probably showing prw-rw----  but should certainly begin with 'p' !!
/bin/vdir $commandfile

printf "\n# if nagios.cmd exists and is a named pipe (see above) then ...\n"
printf "# ...that suggests that we should see in following grep a line ...\n"
printf "# ...similar to: check_external_commands=1  \n"

printf "\n# -v- Grepping of main nagios.cfg file\n"
/bin/egrep '_external|command_check|command_file' /etc/nagios3/nagios.cfg


EGREP_WEB='apache2|lighttpd|httpd|nginx'
printf "\n# -v- Web server packages installed (non-exhaustive)\n"
printf "# ...checks for presence of %s\n" "$EGREP_WEB"

/usr/bin/dpkg -l | /bin/egrep '^ii' | \
/bin/egrep -- ${EGREP_WEB} | \
/usr/bin/tr -s ' ' | /usr/bin/cut -d ' ' -f1-5 | \
/usr/bin/awk -F' ' '{ print $1" "$2"        "$3"   "$4"   "$5 }'

printf "\n# -v- Web server port 80 listen/connections\n"
/bin/netstat -anp -4 -6 | /bin/egrep '\.[[:digit:]]+:80'
printf "\n** end of net check output **\n"

printf "\n# -v- Actively logging to nagios.log?\n"
/usr/bin/find /var/log/nagios3/ -maxdepth 1 -type f -name 'nagios.log' -mtime -2 -ls

printf "\n# -v- Nagios group (and user) setting from main config\n"
/bin/egrep '_user|_group' /etc/nagios3/nagios.cfg

printf "\n# -v- Nagios group (and members) - relevant for nrpe plugin (if installed)\n"

/usr/bin/getent group nagios

# Use following command to verify that it is nrpe that does the creation of 'nagios' group
# /bin/egrep -A2 'adduser|addgroup' /var/lib/dpkg/info/icinga*.postinst /var/lib/dpkg/info/nagios-nrpe*.postinst
# Additionally check nagios_group=nagios

printf "\n*** END OF CHECKS OUTPUT ***\n"

exit 0

#  1158908      4 drwx-ws---   2 nagios   nagios       4096 Sep  7 14:20 /var/lib/nagios3/rw/
#  1159418      0 prw-rw----   1 nagios   nagios          0 Sep  7 14:20 /var/lib/nagios3/rw/nagios.cmd
# Above is typical nagios3 permissions and ownership of nagios.cmd. Next icinga1 example
#   163678      4 drwx------   2 nagios   www-data     4096 Sep  7 14:23 /var/lib/icinga/rw/
#   132826      0 prw-rw----   1 nagios   nagios          0 Sep  7 14:23 /var/lib/icinga/rw/icinga.cmd

# /usr/sbin/nagios3 -v /etc/nagios3/nagios.cfg && systemctl reload nagios3
# /usr/sbin/icinga -v /etc/icinga/icinga.cfg && systemctl reload icinga

# printf '#!/bin/sh\n/usr/bin/icli --config /etc/nagios3/nagios.cfg -f /var/cache/nagios3/status.dat $*\n' >> /usr/local/bin/icli3.sh

